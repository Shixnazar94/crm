<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "status_client".
 *
 * @property integer $id
 * @property string $status_name
 * @property string $color
 *
 * @property Clients[] $clients
 */
class StatusClient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status_client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_name', 'color'], 'required'],
            [['status_name', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_name' => 'Status Name',
            'color' => 'Color',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Clients::className(), ['status_client_id' => 'id']);
    }
}
