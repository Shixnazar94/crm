<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status_client`.
 */
class m180227_162257_create_status_client_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status_client', [
            'id' => $this->primaryKey(),
            'status_name'=>$this->string(255)->notNull(),
            'color'=>$this->string()->notNull(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status_client');
    }
}
