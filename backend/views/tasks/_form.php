<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\time\TimePicker;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Tasks */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="content">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date')->widget(DatePicker::className(),[
        'name' => 'check_issue_date',
        'value' => date('Y-m-d', strtotime('-2 days')),
        'options' => ['placeholder' => 'Select issue date ...'],
        'pluginOptions' => [
            'format' => 'yyyy-m-d',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'time')->widget(TimePicker::className(),[
        'name' => 'start_time',
        'value' => '11:24 AM',
        'pluginOptions' => [
           // 'showSeconds' => true
        ]
    ]); ?>

    <?= $form->field($model, 'status_task_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\common\models\StatusClient::find()->all(),'id','status_name'),
        'language' => 'en',
        'options' => ['placeholder' => 'Statusni tanlang ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\common\models\Users::find()->all(),'id','fio'),
        'language' => 'en',
        'options' => ['placeholder' => 'Statusni tanlang ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);  ?>

    <?= $form->field($model, 'client_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\common\models\Clients::find()->all(),'id','organization_name'),
        'language' => 'en',
        'options' => ['placeholder' => 'Statusni tanlang ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</section>
