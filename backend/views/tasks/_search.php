<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\TasksSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="content">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'date')->textInput()
//        ->widget(DatePicker::className(),[
//        'model' => $model,
//        'attribute' => 'date',
//        //'attribute2' => 'to_date',
//        'options' => ['placeholder' => 'Start date'],
//        //'options2' => ['placeholder' => 'End date'],
//        'type' => DatePicker::TYPE_RANGE,
//        'form' => $form,
//        'pluginOptions' => [
//            'format' => 'yyyy-mm-dd',
//            'autoclose' => true,
//        ]
//    ]);
?>
    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'time') ?>

    <?= $form->field($model, 'status_task_id') ?>

    <?= $form->field($model, 'text') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <?php // echo $form->field($model, 'user_id') ?>

    <?php // echo $form->field($model, 'client_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</section>
