<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\StatusTaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Status Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Status Task', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','class' => 'yii\grid\CheckboxColumn'],

           // 'id',
            'status_name',
            'color',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</section>
