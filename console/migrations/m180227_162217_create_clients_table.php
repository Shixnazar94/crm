<?php

use yii\db\Migration;

/**
 * Handles the creation of table `clients`.
 */
class m180227_162217_create_clients_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('clients', [
            'id' => $this->primaryKey(),
            'organization_name'=>$this->string(255)->notNull(),
            'contact_name'=>$this->string(255)->notNull(),
            'phone'=>$this->string(20)->notNull(),
            'email'=>$this->string(20)->notNull(),
            'user_id'=>$this->integer()->notNull(),
            'address'=>$this->text()->notNull(),
            'group_id'=>$this->integer()->notNull(),
            'status_client_id'=>$this->integer()->notNull(),
            'url'=>$this->string()->notNull(),
            'create_at'=>$this->dateTime()->notNull(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('clients');
    }
}
