<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "status_relation".
 *
 * @property integer $id
 * @property string $status_name
 * @property string $color
 */
class StatusRelation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'status_relation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_name', 'color'], 'required'],
            [['status_name', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_name' => 'Status Name',
            'color' => 'Color',
        ];
    }
}
