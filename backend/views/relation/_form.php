<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\Users;
use common\models\Clients;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Relation */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="content">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-6">
        <?= $form->field($model, 'client_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Clients::find()->all(),'id','organization_name'),
            'language' => 'en',
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

    </div>
    <div class="col-md-6">
        <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Users::find()->all(),'id','fio'),
            'language' => 'en',
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

    </div>
    <div class="col-md-4">

    <?= $form->field($model, 'status_relation_id')->dropDownList([
            ArrayHelper::map(\common\models\StatusClient::find()->all(),'id','status_name')
    ]) ?>

    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'time')->widget(TimePicker::className(),[
            'name' => 'start_time',
            'value' => '11:24 AM',
            'pluginOptions' => [
                // 'showSeconds' => true
            ]
        ]); ?>
    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'date')->widget(DatePicker::className(),[
            'name' => 'check_issue_date',
            'value' => date('Y-m-d', strtotime('-2 days')),
            'options' => ['placeholder' => 'Select issue date ...'],
            'pluginOptions' => [
                'format' => 'yyyy-m-d',
                'todayHighlight' => true
            ]
        ]); ?>
    </div>

    <div class="col-md-12">

        <?= $form->field($model, 'text')->textarea(['rows' => 8]) ?>

    </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</section>
