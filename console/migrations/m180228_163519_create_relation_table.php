<?php

use yii\db\Migration;

/**
 * Handles the creation of table `relation`.
 */
class m180228_163519_create_relation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('relation', [
            'id' => $this->primaryKey(),
            'date'=>$this->date()->notNull(),
            'time'=>$this->time()->notNull(),
            'status_relation_id'=>$this->integer()->notNull(),
            'text'=>$this->text()->notNull(),
            'create_at'=>$this->date()->notNull(),
            'update_at'=>$this->date()->notNull(),
            'user_id'=>$this->integer()->notNull(),
            'client_id'=>$this->integer()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('relation');
    }
}
