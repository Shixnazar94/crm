<?php

namespace common\models;

use Yii;
use common\models\Users;

/**
 * This is the model class for table "clients".
 *
 * @property integer $id
 * @property string $organization_name
 * @property string $contact_name
 * @property string $phone
 * @property string $email
 * @property integer $user_id
 * @property string $address
 * @property integer $group_id
 * @property integer $status_client_id
 * @property string $url
 * @property string $create_at
 *
 * @property Grouping $group
 * @property StatusClient $statusClient
 * @property Users $user
 * @property Tasks[] $tasks
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['organization_name', 'user_id','contact_name', 'phone', 'email', 'user_id', 'address', 'group_id', 'status_client_id', 'url'], 'required'],
            [['group_id', 'status_client_id'], 'integer'],
            [['address'], 'string'],
            [['create_at'], 'safe'],
            [['phone'], 'match','pattern'=>'/^\+\9\9\8\(\d{2}\)\-\d{3}\-\d{2}\-\d{2}$/','message'=>'+998(97)-211-56-44 shu tartibda kiriting'],
            ['email','email'],
            [['organization_name', 'contact_name', 'url'], 'string', 'max' => 255],
            [['phone', 'email'], 'string', 'max' => 20],
           // [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => Grouping::className(), 'targetAttribute' => ['group_id' => 'id']],
            //[['status_client_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatusClient::className(), 'targetAttribute' => ['status_client_id' => 'id']],
           // [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'organization_name' => 'Organization Name',
            'contact_name' => 'Contact Name',
            'phone' => 'Phone',
            'email' => 'Email',
            'user_id' => 'User Name',
            'address' => 'Address',
            'group_id' => 'Group Name',
            'status_client_id' => 'Status Client Name',
            'url' => 'Url',
            'create_at' => 'Create At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Grouping::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusClient()
    {
        return $this->hasOne(StatusClient::className(), ['id' => 'status_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\common\models\Users::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Tasks::className(), ['client_id' => 'id']);
    }
}
