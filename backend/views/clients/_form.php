<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use common\models\Users;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Clients */
/* @var $form yii\widgets\ActiveForm */
?>

<section class="content">

    <?php $form = ActiveForm::begin(); ?>
        <div class="col-md-4">
            <?= $form->field($model, 'organization_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
        <?= $form->field($model, 'contact_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">

        <?= $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(), [
            'mask' => '+999(99)-999-99-99'
        ]) ?>
        </div>
        <div class="col-md-4">

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        </div>

        <div class="col-md-4">

        <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Users::find()->all(),'id','fio'),
            'language' => 'en',
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'address')->textarea(['rows' => 4]) ?>
        </div>

        <div class="col-md-4">

    <?= $form->field($model, 'group_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\common\models\Grouping::find()->all(),'id','group_name'),
        'language' => 'en',
        'options' => ['placeholder' => 'Groupni tanlang ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

        </div>

        <div class="col-md-4">

    <?= $form->field($model, 'status_client_id')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(\common\models\StatusClient::find()->all(),'id','status_name'),
        'language' => 'en',
        'options' => ['placeholder' => 'Statusni tanlang ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
        </div>

        <div class="col-md-4">

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

        </div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Изменить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</section>
