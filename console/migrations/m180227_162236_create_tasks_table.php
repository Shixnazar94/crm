<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tasks`.
 */
class m180227_162236_create_tasks_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tasks', [
            'id' => $this->primaryKey(),
            'date'=>$this->date()->notNull(),
            'time'=>$this->time()->notNull(),
            'status_task_id'=>$this->integer()->notNull(),
            'text'=>$this->text()->notNull(),
            'create_at'=>$this->date()->notNull(),
            'update_at'=>$this->date()->notNull(),
            'user_id'=>$this->integer()->notNull(),
            'client_id'=>$this->integer()->notNull(),
        ]);

    }



    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tasks');
    }
}
