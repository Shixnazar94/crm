<?php

use yii\db\Migration;

class m180228_164216_add_foreign_key extends Migration
{
     public function safeUp()
    {
        $this->createIndex('idx-relation-user_id','relation','user_id',false);
        $this->createIndex('idx-relation-status_relation_id','relation','status_relation_id',false);
        $this->createIndex('idx-relation-client_id','relation','client_id',false);

        $this->addForeignKey('fk-relation-user_id','relation','user_id','user','id');
        $this->addForeignKey('fk-relation-status_relation_id','relation','status_relation_id','status_relation','id');
        $this->addForeignKey('fk-relation-client_id','relation','client_id','clients','id');


    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-relation-user_id','tasks');
        $this->dropForeignKey('fk-relation-client_id','tasks');
        $this->dropForeignKey('fk-relation-satatus_task_id','tasks');
        $this->dropIndex('idx-relation-user_id','tasks');
        $this->dropIndex('idx-relation-client_id','tasks');
        $this->dropIndex('idx-relation-status_task_id','tasks');

    }
}
