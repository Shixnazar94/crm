<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tasks".
 *
 * @property integer $id
 * @property string $date
 * @property string $time
 * @property integer $status_task_id
 * @property string $text
 * @property string $create_at
 * @property string $update_at
 * @property integer $user_id
 * @property integer $client_id
 *
 * @property Clients $client
 * @property StatusTask $statusTask
 * @property Users $user
 */
class Tasks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tasks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'time', 'status_task_id', 'text', 'create_at', 'update_at', 'user_id', 'client_id'], 'required'],
            [['date', 'time', 'create_at', 'update_at'], 'safe'],
            [['status_task_id', 'user_id', 'client_id'], 'integer'],
            [['text'], 'string'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['status_task_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatusTask::className(), 'targetAttribute' => ['status_task_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'time' => 'Time',
            'status_task_id' => 'Status Task ID',
            'text' => 'Text',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
            'user_id' => 'User ID',
            'client_id' => 'Client ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusTask()
    {
        return $this->hasOne(StatusTask::className(), ['id' => 'status_task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
}
