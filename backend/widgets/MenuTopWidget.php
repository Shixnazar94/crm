<?php

namespace backend\widgets;

use yii\base\Widget;

class MenuTopWidget extends Widget{
    public function run()
    {
        return $this->render('menu-top');
    }
}