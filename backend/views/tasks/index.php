<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Clients;
use common\models\Users;
use yii\helpers\ArrayHelper;
use common\models\StatusClient;
/* @var $this yii\web\View */
/* @var $searchModel common\models\TasksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tasks';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tasks', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions'=>function($model){
        return ['style'=>'background-color: '.$model->statusTask->color.";"];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
              'attribute'=>'client_id',
              'label'=>'Клиент',
              'value'=>function($dataProvider){
                    $d=Html::a(Html::encode($dataProvider->client->organization_name),['clients/view','id'=>$dataProvider->client->id]);
                return $d;
              },
                'format'=>'raw',
                'filter'=>ArrayHelper::map(Clients::find()->asArray()->all(),'id','organization_name')
            ],
            // 'user_id',
            [
              'attribute'=>'user_id',
              'label'=>'Менеджер',
              'value'=>'user.fio',
              'filter'=>ArrayHelper::map(Users::find()->asArray()->all(),'id','fio')
            ],
            //'status_task_id',
            [
              'attribute'=>'status_task_id',
              'value'=>'statusTask.status_name',
                'label'=>'Статус',
                'filter'=>ArrayHelper::map(StatusClient::find()->asArray()->all(),'id','status_name')
            ],
            [
              'attribute'=>'time',
              'label'=>'Время',
            ],
            [
                    'attribute'=>'date',
                    'label'=>'Дата'
            ],
            [
              'attribute'=>'text',
              'label'=>'Текст'
            ],
            // 'create_at',
            // 'update_at',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</section>
