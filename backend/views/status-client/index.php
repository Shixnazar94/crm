<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\StatusClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Status Clients';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Status Client', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        //'layout'=>"{sorter}\n{pager}\n{summary}\n{items}",


        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id',
            'status_name',
             'color',


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</section>
