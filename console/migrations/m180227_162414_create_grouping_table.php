<?php

use yii\db\Migration;

/**
 * Handles the creation of table `grouping`.
 */
class m180227_162414_create_grouping_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('grouping', [
            'id' => $this->primaryKey(),
            'group_name'=>$this->string()->notNull(),
            'color'=>$this->string()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('grouping');
    }
}
