<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "grouping".
 *
 * @property integer $id
 * @property string $group_name
 * @property string $color
 *
 * @property Clients[] $clients
 */
class Grouping extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'grouping';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_name', 'color'], 'required'],
            [['group_name', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_name' => 'Group Name',
            'color' => 'Color',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClients()
    {
        return $this->hasMany(Clients::className(), ['group_id' => 'id']);
    }
}
