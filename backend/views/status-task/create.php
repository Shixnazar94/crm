<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\StatusTask */

$this->title = 'Create Status Task';
$this->params['breadcrumbs'][] = ['label' => 'Status Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</section>
