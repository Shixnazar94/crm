<?php

use yii\db\Migration;

class m180227_191113_add_foreign_keys extends Migration
{

    public function safeUp()
    {
        $this->createIndex('idx-tasks-user_id','tasks','user_id',false);
        $this->createIndex('idx-tasks-client_id','tasks','client_id',false);
        $this->createIndex('idx-tasks-status_task_id','tasks','status_task_id',false);

        $this->addForeignKey('fk-tasks-user_id','tasks','user_id','user','id');
        $this->addForeignKey('fk-tasks-client_id','tasks','client_id','clients','id');
        $this->addForeignKey('fk-tasks-satatus_task_id','tasks','status_task_id','status_task','id');


    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-tasks-user_id','tasks');
        $this->dropForeignKey('fk-tasks-client_id','tasks');
        $this->dropForeignKey('fk-tasks-satatus_task_id','tasks');
        $this->dropIndex('idx-tasks-user_id','tasks');
        $this->dropIndex('idx-tasks-client_id','tasks');
        $this->dropIndex('idx-tasks-status_task_id','tasks');
    }
}
