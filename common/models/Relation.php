<?php

namespace common\models;

use Yii;
use common\models\User;
use common\models\Grouping;
/**
 * This is the model class for table "relation".
 *
 * @property integer $id
 * @property string $date
 * @property string $time
 * @property integer $status_relation_id
 * @property string $text
 * @property string $create_at
 * @property string $update_at
 * @property integer $user_id
 * @property integer $client_id
 *
 * @property Clients $client
 * @property StatusRelation $statusRelation
 * @property Users $user
 */
class Relation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'relation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'time', 'status_relation_id', 'text', 'user_id', 'client_id'], 'required'],
            [['date', 'time', 'create_at', 'update_at'], 'safe'],
            [['status_relation_id', 'user_id', 'client_id'], 'integer'],
            [['text'], 'string'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clients::className(), 'targetAttribute' => ['client_id' => 'id']],
            [['status_relation_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatusRelation::className(), 'targetAttribute' => ['status_relation_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата',
            'time' => 'Время',
            'status_relation_id' => 'Отношения',
            'text' => 'Текст',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
            'user_id' => 'Менежер',
            'client_id' => 'Клиент',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Clients::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusRelation()
    {
        return $this->hasOne(StatusRelation::className(), ['id' => 'status_relation_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['id' => 'user_id']);
    }

}
