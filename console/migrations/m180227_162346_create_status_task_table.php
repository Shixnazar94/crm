<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status_task`.
 */
class m180227_162346_create_status_task_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status_task', [
            'id' => $this->primaryKey(),
            'status_name'=>$this->string()->notNull(),
            'color'=>$this->string()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status_task');
    }
}
