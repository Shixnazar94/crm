<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use common\models\Users;
use common\models\Grouping;
use common\models\StatusClient;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ClientsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clients';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content col-md-12">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?=Html::button('Добавить',['value'=>\yii\helpers\Url::to(['/clients/create']),'class'=>'btn btn-success pull-right','id'=>'ModalButton'])?>
    </p>
    <?php
    Modal::begin([
            'header'=>'<h1 class="text-center text-info">Создать</h1>',
            'id'=>'modal',
            'size'=>'modal-lg'
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' =>
            function ($model, $key, $index, $grid)
            {
                return ['style' => 'background-color:'.$model->statusClient->color. ";"];
            },

        'columns' => [

            ['class' => 'yii\grid\SerialColumn','class' => 'yii\grid\CheckboxColumn'],

           // 'id',
            [
              'attribute'=>'organization_name',
                'label'=>'Название Организации',
            ],
            [
            'attribute'=>'contact_name',
             'label'=>'Контакт'
            ],
            [
                'attribute'=>'phone',
                'label'=>'Телефон'
            ],
            [
                'attribute'=>'email',
                'label'=>'Почта'
            ],


            [
              'attribute'=>'user_id',
                'label'=>'Менеджер',
              'value'=>'user.fio',
                'filter'=>ArrayHelper::map(Users::find()->asArray()->all(),'fio','fio'),
            ],

            [
              'attribute'=>'group_id',
                'label'=>'Группа',
              'value'=>'group.group_name',
               'filter'=>ArrayHelper::map(Grouping::find()->asArray()->all(),'group_name','group_name'),
            ],

            [
              'attribute'=>'status_client_id',
                'label'=>'Статус',
              'value'=>'statusClient.status_name',
                'filter'=>ArrayHelper::map(StatusClient::find()->asArray()->all(),'status_name','status_name')
            ],
            [
                'attribute'=>'address',
                'label'=>'Адрес'
            ],
            [
                'attribute'=>'url',
                'label'=>'Ссылка'
            ],

            // 'create_at',

            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{delete}',

            ],

        ],
    ]); ?>

    <?
    $js=<<<JS
$(function() {
  $('#ModalButton').click(function() {
    $('#modal').modal('show').find('#modalContent').load($(this).attr('value'));
  });
});
JS;
$this->registerJs($js);
    ?>
</section>
