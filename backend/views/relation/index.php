<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use common\models\Users;
use common\models\Clients;
use common\models\StatusRelation;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $searchModel common\models\RelationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Relations';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?=Html::button('Добавить',['value'=>\yii\helpers\Url::to(['/relation/create']),'class'=>'btn btn-success pull-right','id'=>'ModalButton'])?>
    </p>
    <?php
    Modal::begin([
        'header'=>'<h1 class="text-center text-info">Создать</h1>',
        'id'=>'modal',
        'size'=>'modal-lg'
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' =>
            function ($model, $key, $index, $grid)
            {
                return ['style' => 'background-color:'.$model->statusRelation->color. ";"];
            },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn','class' => 'yii\grid\CheckboxColumn'],

            //'id',
            [
              'attribute'=>'client_id',
              'label'=>'Клиент',
                'value'=>'client.organization_name',
                'filter'=>ArrayHelper::map(Clients::find()->asArray()->all(),'id','organization_name')
            ],

            [
              'attribute'=>'user_id',
              'label'=>'Менежер',
               'value'=>'user.fio',
                'filter'=>ArrayHelper::map(Users::find()->asArray()->all(),'id','fio')
            ],

            [
              'attribute'=>'status_relation_id',
              'label'=>'Отношения',
                'value'=>'statusRelation.status_name',
                'filter'=>ArrayHelper::map(StatusRelation::find()->asArray()->all(),'id','status_name')
            ],
            [
                    'attribute'=>'time',
                    'label'=>'Время',
            ],
            [
              'attribute'=>'date',
              'value'=>'date',
              'label'=>'Дата',
              'filter'=>\kartik\date\DatePicker::widget([
                      'model'=>$searchModel,
                        'attribute'=>'date',
                        //'template'=>'{addon}{input}',
                        'pluginOptions'=>[
                                'autoClose'=>true,
                                'format'=>'yyyy-m-d'
                        ]
              ])
            ],
            'text:ntext',
            // 'create_at',
            // 'update_at',


            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update}{delete}'
            ],

        ],
    ]); ?>
    <?
    $js=<<<JS
$(function() {
  $('#ModalButton').click(function() {
    $('#modal').modal('show').find('#modalContent').load($(this).attr('value'));
  });
});
JS;
    $this->registerJs($js);
    ?>
</section>
