<?php

use yii\db\Migration;

class m180227_190610_add_foreign_keys extends Migration
{

    public function safeUp()
    {
        $this->createIndex('idx-clients-user_id','clients','user_id',false);
        $this->createIndex('idx-clients-group_id','clients','group_id',false);
        $this->createIndex('idx-clients-status_client_id','clients','status_client_id',false);

        $this->addForeignKey('fk-clients-user_id','clients','user_id','user','id');
        $this->addForeignKey('fk-clients-group_id','clients','group_id','grouping','id');
        $this->addForeignKey('fk-clients-status_client_id','clients','status_client_id','status_client','id');

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-clients-user_id','clients');
        $this->dropForeignKey('fk-clients-group_id','clients');
        $this->dropForeignKey('fk-clients-status_client_id','clients');
        $this->dropIndex('idx-clients-user_id','clients');
        $this->dropIndex('idx-clients-group_id','clients');
        $this->dropIndex('idx-clients-status_client_id','clients');
    }

}
