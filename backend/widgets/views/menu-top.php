<?
use yii\bootstrap\ActiveForm;
?>
<?=\yii\bootstrap\Nav::widget([
    'options'=>['class'=>'navbar-nav navbar-left pull-right-1 '],
        'encodeLabels' => false,
    'items' => [

        // Important: you need to specify url as 'controller/action',
        // not just as 'controller' even if default action is used.
        ['label' => '<span class="glyphicon glyphicon-home"></span> Рабочий стол', 'url' => ['/site']],
        ['label' => '<span class="glyphicon glyphicon-user"></span> Клиенты', 'url' => ['/clients']],
        ['label' => 'Отношения', 'url' => ['/relation', 'tag' => 'popular']],
        // 'Products' menu item will be selected as long as the route is 'product/index'
            ['label' => 'Задачи', 'url' => ['/tasks', 'tag' => 'new']],

        ['label' => 'Справочники', 'url' => ['/'], 'items' => [
            ['label' => '', 'url' => ['/status-client', 'tag' => 'new']],
            ['label' => '<span class="fa fa-users"></span> Пользователи', 'url' => ['/users', 'tag' => 'new']],
            ['label' => 'Статусы Client', 'url' => ['/status-client', 'tag' => 'popular']],
            ['label' => 'Статусы Отношений', 'url' => ['/status-relation', 'tag' => 'popular']],
            ['label' => 'Статусы Задач', 'url' => ['/status-task', 'tag' => 'popular']],
            ['label' => 'Группировка', 'url' => ['/grouping', 'tag' => 'popular']],
        ]],
        ['label' => '', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
    ],
]);?>
