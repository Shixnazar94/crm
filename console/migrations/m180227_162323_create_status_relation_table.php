<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status_relation`.
 */
class m180227_162323_create_status_relation_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status_relation', [
            'id' => $this->primaryKey(),
            'status_name'=>$this->string()->notNull(),
            'color'=>$this->string()->notNull()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status_relation');
    }
}
