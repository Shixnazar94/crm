<?
namespace backend\views\clients\clientview;
use yii\helpers\Html;
use yii\bootstrap\Modal;
?>
<?
Modal::begin([
    'header'=>'<h1 class="text-center text-info">Изменить</h1>',
    'id'=>'modal',
    'size'=>'modal-lg'
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>
<section class="content">
    <br>
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Информация</a></li>

        <li><a data-toggle="tab" href="#menu1">Отношения </a></li>
        <li><a data-toggle="tab" href="#menu2">Задачи </a></li>
    </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            <h3>Общие элементы <?=Html::button('<span class="fa fa-pencil"></span>',['value'=>\yii\helpers\Url::to(['/clients/update','id'=>$model->id]),'class'=>'btn btn-success btn-sm ','id'=>'ModalButton'])?></h3>
            <table class="table table-hover table-striped" border="1">
                <tr>
                    <td><b>Название Организации</b></td>
                    <td><?=$model->organization_name?></td>
                    <td><b>Контакт</b></td>
                    <td><?=$model->contact_name?></td>
                </tr>
                <tr>
                    <td><b>Дата создания</b></td>
                    <td><?=$model->create_at?></td>
                    <td><b>Телефон</b></td>
                    <td><?=$model->phone?></td>
                </tr>
                <tr>
                    <td><b>Почта</b></td>
                    <td><?=$model->email?></td>
                    <td><b>Менеджер</b></td>
                    <td><?=$model->user->fio?></td>
                </tr>
                <tr>
                    <td><b>Группа</b></td>
                    <td><?=$model->group->group_name?></td>
                    <td><b>Адрес</b></td>
                    <td><?=$model->address?></td>
                </tr>
                <tr>
                    <td><b>Статус</b></td>
                    <td><?=$model->statusClient->status_name?></td>
                    <td><b>Ссылка</b></td>
                    <td><?=$model->url?></td>
                </tr>
            </table>
        </div>
        <div id="menu1" class="tab-pane fade">
            <h2>Резултать</h2>
            <table class="table table-striped table-bordered" border="2">
                <tr>
                    <th>Дата</th>
                    <th>Время</th>
                    <th>Статус</th>
                    <th>Текст</th>
                    <th>Группа</th>
                </tr>
                <? foreach ($relation as $one): ?>
                <tr style="background: <?=$one->statusRelation->color?>">
                    <td><?=$one->date?></td>
                    <td><?=$one->time?></td>
                    <td><?=$one->statusRelation->status_name?></td>
                    <td><?=$one->text?></td>
                    <td><?=$one->client->group->group_name?></td>
                </tr>
                <? endforeach;?>
            </table>
        </div>
        <div id="menu2" class="tab-pane fade">
            <h3>Резултать</h3>
            <table class="table table-hover" border="1">
                <th>Дата</th>
                <th>Время</th>
                <th>Статус</th>
                <th>Текст</th>
                <th>Группа</th>
            <? foreach ($tasks as $one):?>
                <tr style="background: <?=$one->statusTask->color?>">
                <td><?=$one->date?></td>
                <td><?=$one->time?></td>
                <td><?=$one->statusTask->status_name?></td>
                <td><?=$one->text?></td>
                <td><?=$one->client->group->group_name?></td>
                </tr>
                <? endforeach;?>
            </table>
        </div>
    </div>
    <?
    $js=<<<JS
$(function() {
  $('#ModalButton').click(function() {
    $('#modal').modal('show').find('#modalContent').load($(this).attr('value'));
  });
});
JS;
    $this->registerJs($js);
    ?>
</section>
